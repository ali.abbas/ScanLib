#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CadreView.h"
#import "CroppingArea.h"
#import "CropView.h"
#import "PhotoView.h"
#import "Process.h"
#import "ScanLib.h"

FOUNDATION_EXPORT double ScanLibVersionNumber;
FOUNDATION_EXPORT const unsigned char ScanLibVersionString[];

