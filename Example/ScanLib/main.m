//
//  main.m
//  ScanLib
//
//  Created by ruben Boboti on 08/07/2018.
//  Copyright (c) 2018 ruben Boboti. All rights reserved.
//

@import UIKit;
#import "AuchanAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AuchanAppDelegate class]));
    }
}
