//
//  AuchanViewController.m
//  ScanLib
//
//  Created by ruben Boboti on 08/07/2018.
//  Copyright (c) 2018 ruben Boboti. All rights reserved.
//

#import "AuchanViewController.h"

@interface AuchanViewController () <ScanLibDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageTest;

@property( strong, nonatomic) ScanLib * lib;
@end

@implementation AuchanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.lib          = [[ScanLib alloc] init];
    self.lib.delegate = self;
    
    [self presentViewController:self.lib animated:YES completion:^{
        [self.lib openCamera];
        NSLog(@"%@", m_result);
    }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scanDone:(NSString *)cropping_area WithImage:(UIImage *)imageRecipe {
    [self dismissViewControllerAnimated:true completion:^{
        
        NSLog(@"%@", cropping_area);
        if(imageRecipe != nil)
        {
            self.imageTest.image = imageRecipe;
        }
    }];
    
}



@end
