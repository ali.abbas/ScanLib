//
//  AuchanAppDelegate.h
//  ScanLib
//
//  Created by ruben Boboti on 08/07/2018.
//  Copyright (c) 2018 ruben Boboti. All rights reserved.
//

@import UIKit;

@interface AuchanAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
