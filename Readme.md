# ScanLib

[![CI Status](https://img.shields.io/travis/ruben Boboti/ScanLib.svg?style=flat)](https://travis-ci.org/ruben Boboti/ScanLib)
[![Version](https://img.shields.io/cocoapods/v/ScanLib.svg?style=flat)](https://cocoapods.org/pods/ScanLib)
[![License](https://img.shields.io/cocoapods/l/ScanLib.svg?style=flat)](https://cocoapods.org/pods/ScanLib)
[![Platform](https://img.shields.io/cocoapods/p/ScanLib.svg?style=flat)](https://cocoapods.org/pods/ScanLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ScanLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ScanLib'
```

## Author

ruben Boboti, ruben.boboti@orange.com

## License

ScanLib is available under the MIT license. See the LICENSE file for more info.
