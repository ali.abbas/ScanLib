//
//  Traitment.h
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//
//

#import <Foundation/Foundation.h>
#import "ScanLib.h" 
@interface Process : NSObject

//---------------------------------------------------------------------------------------------------------------------------------

- (NSString *)process;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void saveImage
 * \brief launch saveImageInBackground function.
 *
 * This function allows to call saveImage function in background.
 */
- (void)saveImage;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void saveImageInBackground
 * \brief saving the image in background.
 *
 * This function allows to save the image.
 */
- (void)saveImageInBackground;

//---------------------------------------------------------------------------------------------------------------------------------

@end
