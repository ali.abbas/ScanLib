//
//  PhotoView.h
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 22/04/2016.
//
//

#import <UIKit/UIKit.h>
#import "CadreView.h"

@import AVFoundation;

@interface PhotoViewController : UIViewController

@property(nonatomic, strong) UIView * preview; ///< a UIView variable used to store and display the preview of the camera.
@property(nonatomic, strong) UIButton * cancelButton; ///< a UIButton variable which allows to the user to cancel the view when the camera view is open.
@property(nonatomic, strong) UIButton * photoButton; ///< a UIButton variable which allows to the user to capture an Image with the button when the camera view is open.

@property (nonatomic, strong) UIImage * redimImg; ///< a UIImage used to store the resized ImageView container.

@property (nonatomic) float orig_w ; ///< a float variable used to store the original width of the ImageView container.
@property (nonatomic) float orig_h ; ///< a float variable used to store the original height of the ImageView container.
@property (nonatomic) float redim_w ; ///< a float variable used to store the resized width of the ImageView container.
@property (nonatomic) float redim_h ; ///< a float variable used to store the resized height of the ImageView container.

@property(nonatomic, retain) AVCaptureStillImageOutput *stillImageOutput; ///< a AVCaptureStillImageOutput

@property (nonatomic) AVCaptureDevice * cam; ///< a AVCaptureDevice variable containing the camera.
@property (nonatomic) AVCaptureSession * session; ///< a AVCaptureSession variable containing the session of the camera.

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn timerTicked:(NSTimer*)timer
 * \brief PhotoViewController controler.
 *
 * This function allows to control what the Photo view controller should do.
 */
- (void)timerTicked:(NSTimer*)timer;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn pretakePicture:(id)sender
 * \brief pretakePicture controler.
 *
 * This function is called when an image will be taken.
 *
 * \param sender an id variable.
 */
- (void)pretakePicture:(id)sender;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn takePicture
 * \brief take picture.
 *
 * This function allows to take a picture.
 */
- (void)takePicture;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn cancelTakePicture:(id)sender
 * \brief cancel taking picture.
 *
 * This function allows to cancel the taking of picture.
 *
 * \param sender an id variable.
 */
- (void)cancelTakePicture:(id)sender;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn validPicture:(UIImage *)img
 * \brief valid the image.
 *
 * This function allows to valid the image and go to the CropView view.
 *
 * \param img an UIImage variable containing the valid image.
 */
- (void)validPicture:(UIImage *)img;

@end
