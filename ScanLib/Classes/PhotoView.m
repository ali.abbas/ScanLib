//
//  PhotoViewController.m
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 22/04/2016.
//
//

#import <ImageIO/CGImageProperties.h>
#import <ImageIO/CGImageMetadata.h>
#import "CropView.h"
#import "ScanLib.h" 

@interface PhotoViewController ()

@end

@implementation PhotoViewController
{
    NSTimer * timerWaitTxt; ///< a NSTimer variable which is set when a picture is captured.
    NSTimer * timerWaitFocus; ///< a NSTimer variable which is set when pretakePicture is called.
}

@synthesize photoButton, cancelButton, preview;

UIImageView *m_valid_image; ///< a UIImageView variable used to store the valid image captured.

//---------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[[UIApplication sharedApplication] setStatusBarHidden:true];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    self.preview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    self.preview.backgroundColor = [UIColor blackColor];
    self.preview.hidden = false;
    [self.view addSubview:self.preview];
    
    self.photoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, height-32)];
    [self.photoButton addTarget:self action:@selector(pretakePicture:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.photoButton];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.frame = CGRectMake(0, height-24, width, 24);
    self.cancelButton.opaque = false ;
    [self.cancelButton setTitle:(NSString *)@"Annuler" forState:(UIControlState)UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(cancelTakePicture:) forControlEvents:UIControlEventTouchUpInside];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:self.cancelButton];
    
    m_valid_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height - m_rot_button_height)];
    m_valid_image.contentMode = UIViewContentModeScaleAspectFit ;
    m_valid_image.backgroundColor = [UIColor blackColor];
    m_valid_image.hidden = true;
    [self.view addSubview:m_valid_image];
    
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    //CALayer *viewLayer = self.preview.layer;
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    
    previewLayer.frame = self.view.bounds;
    previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [self.preview.layer addSublayer:previewLayer];
    self.cam = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:self.cam error:&error];
    if (!input) {
        // Handle the error appropriately.
        NSLog(@"ERROR: trying to open camera: %@", error);
    }
    [self.session addInput:input];
    
    AVCaptureDeviceFormat *bestFormat = nil;
    for ( AVCaptureDeviceFormat *format in [self.cam formats] )
    {
        if ( format.accessibilityFrame.size.height * format.accessibilityFrame.size.width > bestFormat.accessibilityFrame.size.height * bestFormat.accessibilityFrame.size.width)
        {
            bestFormat = format;
        }
    }
    
    if ( bestFormat )
    {
        if ( [self.cam lockForConfiguration:NULL] == YES )
        {
            self.cam.activeFormat = bestFormat;
            
            if([self.cam isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus])
            {
                self.cam.focusMode = AVCaptureFocusModeContinuousAutoFocus ;
            }
            
            self.cam.flashMode = AVCaptureFlashModeOff ;
            [self.cam unlockForConfiguration];
        }
    }
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary * outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    
    [self.stillImageOutput setOutputSettings:outputSettings];
    [self.session addOutput:self.stillImageOutput];
    [self.session startRunning];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of my ressources tha can be created
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)timerTicked:(NSTimer*)timer
{
    if(timer == timerWaitTxt)
    {
        [self validPicture:self.redimImg];
    }
    else if(timer == timerWaitFocus)
    {
        if(self.cam.isAdjustingFocus == false)
        {
            [timerWaitFocus invalidate];
            
            if ( [self.cam lockForConfiguration:NULL] == YES )
            {
                if ([self.cam isFocusModeSupported:AVCaptureFocusModeAutoFocus])
                {
                    self.cam.focusMode = AVCaptureFocusModeAutoFocus ;
                }
                [self.cam unlockForConfiguration];
            }
            [self takePicture];
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)pretakePicture:(id)sender
{
    timerWaitFocus = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)takePicture
{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in self.stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    
    NSLog(@"about to request a capture from: %@", self.stillImageOutput);
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments)
         {
             // Do something with the attachments.
         }
         
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         self.redimImg = [[UIImage alloc] initWithData:imageData];
         
         self.photoButton.hidden = true ;
         self.cancelButton.hidden = true ;
         m_valid_image.hidden = false;
         self.photoButton.enabled = false ;
         self.cancelButton.enabled = false ;
         
         timerWaitTxt = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerTicked:) userInfo:nil repeats:NO];
     }];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)cancelTakePicture:(id)sender
{
    [[self presentingViewController] dismissViewControllerAnimated:true completion:nil];
}

//---------------------------------------------------------------------------------------------------------------------------------

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

//---------------------------------------------------------------------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return NO;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)validPicture:(UIImage *)img
{
    m_img_to_save = img;
    [[self presentingViewController] dismissViewControllerAnimated:true completion:nil];
    m_image_taken = true;
}

//---------------------------------------------------------------------------------------------------------------------------------

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

