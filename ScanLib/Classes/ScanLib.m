//
//  ScanLib.m
//  Pods-ScanLib_Example
//
//  Created by ruben Boboti on 07/08/2018.
//

#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <UIKit/UIKit.h>
#include "math.h"
#import "CropView.h"
#import "ScanLib.h"

@implementation ScanLib
{
    PhotoViewController * pvc; ///< a PhotoViewController variable used to open the PhotoViewController camera.
    CropView * cropView;
}


- (id)init
{
    NSLog(@"init");
    
    pvc = nil ;
    self = [super init];
    
    
    NSString *auth_tr = [NSString stringWithFormat:@"%@:%@", @"X0L0N-6WCC8-19EC1-TY4O6-W36HV", @"ZFXFBHYQGPNHQSI"];
    NSData *auth_data = [auth_tr dataUsingEncoding:NSUTF8StringEncoding];
    m_auth_value = [NSString stringWithFormat:@"Basic %@", [auth_data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    
    
    return self;
}



CadreView *cadrImg; ///< a CadreView used to store the container of the crop which will be draw.
NSData *m_image_data = nil; ///< a NSData variable used to store the data of the image to process.

UIImage
*m_img_to_save, ///< an UIImage variable used to store original image captured or taken from the gallery for the saving of the full quality image.
*m_img; ///< an UIImage variable used to store original image in the right orientation. This image is used to display the image in the right orientation to the user.

id m_cropping; ///< an id variable used to store the result of the process for the crop.

/*extern NSString
*m_country,
*m_result,
*m_background_process_ok_with_error,
*m_language,
*m_file_path,
*m_file_path_crop,
*m_auth_value,
*m_token;*/


NSString
*m_country = @"FR", ///< a NSString variable used to store the country's code. Default is "FR".
*m_result,
*m_auth_value,  
*m_token,
*m_language = @"FR", ///< a NSString variable used to store the language's code. Default is "FR".
*m_file_name, ///< a NSString variable used to store the file name.
*m_file_path, ///< a NSString variable used to store the file path.
*m_file_path_crop, ///< a NSString variable used to store crop file path.
*m_document_directory, ///< a NSString variable used to store the document directory.
*m_background_process_ok_with_error = @"", ///< a NSString variable used to store the error's description.
*m_time_name ; ///< a NSString variable used to store the time when a picture is captured or taken from the gallery. This is the name of the file without the extention.

bool
m_rotation_ok = false,
m_background_process_ok, ///< a bool variable used to know if the background process is done or not.
m_from_pdf_file = false, ///< a bool variable used to know if the image cames from a pdf or not.
m_have_internet = false, ///< a bool variable ised to know if we have an internet connection or not.
m_orig_img_saved, ///< a bool variable used to know if the original image have been saved or not.
m_image_taken = false, ///< a a bool variable used to know if the image has been taken or not.
m_crop, ///< a a bool variable used to know if we are in crop mode or in scan mode.
m_from_crop ; ///< a a bool variable used to know if we have done a crop or not.


//---------------------------------------------------------------------------------------------------------------------------------

- (void)openCamera
{
    NSLog(@"openCamera");
    
    [self initializePath];
    m_image_data = nil;
    
    pvc = [[PhotoViewController alloc] init];
    
    [self presentViewController:pvc animated:false completion:nil];
    
    [self waitUntilImageIsTaken];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)waitUntilImageIsTaken
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(100 * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^
                   {
                       if(m_image_taken)
                       {
                           NSLog(@"waitUntilImageIsTaken");
                           
                          // Process *p = [[Process alloc] init];
                           [self saveImage];
                           
                           m_image_taken = false;
                           m_img = [self rotation_image:m_img_to_save withAngle:90];
                          // [m_tools loadingSpinner:self.view:false];
                           self->cropView = [[CropView alloc] init];
                           self->cropView.delegate = self; 
                           [self presentViewController:self->cropView animated:YES completion:^{
                               //[self.lib openCamera];
                           }];
                           //[self performSegueWithIdentifier:@"CropView" sender:self];
                       }
                       else
                       {
                           [self waitUntilImageIsTaken];
                       }
                   });
}
//---------------------------------------------------------------------------------------------------------------------------------


-(void) croppingPoints:(NSString *)cropping_area WithImage:(UIImage *)imageRecipe
{
    NSLog(@"%@",cropping_area);
    if(imageRecipe != nil)
    {
        NSLog(@"imageReady %@", imageRecipe); 
    }
    
    [self.delegate scanDone:cropping_area WithImage:imageRecipe]; 
}


//---------------------------------------------------------------------------------------------------------------------------------

- (void)initializePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    m_document_directory = paths.firstObject;
    m_time_name = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    m_file_name = [NSString stringWithFormat:@"%@.jpg", m_time_name];
    m_file_path = [NSString stringWithFormat:@"%@/%@", m_document_directory, m_file_name];
    m_file_path_crop = [NSString stringWithFormat:@"%@/%@", m_document_directory, [NSString stringWithFormat:@"%@_crop.jpg", m_time_name]];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)_picker
{
    NSLog(@"imagePickerControllerDidCancel");
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)imagePickerController:(UIImagePickerController *)_picker didFinishPickingMediaWithInfo:(NSDictionary *)_info
{
    NSLog(@"imagePickerController");
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [_picker dismissViewControllerAnimated:YES completion:nil];
    
    //stocke l'image choisie
    m_img_to_save = [_info valueForKey:UIImagePickerControllerOriginalImage];
    //m_img = m_img_to_save;
    m_image_taken = true;
    
    [self waitUntilImageIsTaken];
}

//---------------------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------------------

- (void)chooseGallery
{
    NSLog(@"chooseFromGallery");
    
    m_image_data = nil;
    
    [self initializePath];
    
  //  [m_tools loadingSpinner:self.view:true];
    
    m_ipc = [[UIImagePickerController alloc] init];
    m_ipc.delegate = self;
    m_ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    m_ipc.modalPresentationStyle = UIModalPresentationPopover;
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone){
            [self presentViewController:m_ipc animated:YES completion:nil];
    }
}

- (void)choosePDF
{
    NSLog(@"choosePDF");
    
    m_image_data = nil;
    
    [self initializePath];
    NSString *_pdf_bundle = [[NSBundle mainBundle] pathForResource:@"sample_invoice" ofType:@"pdf"];
    
    m_from_pdf_file = true;
    m_crop = false;
    m_image_data = [NSData dataWithContentsOfFile:_pdf_bundle];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    m_document_directory = paths.firstObject;
    m_time_name = [NSString stringWithFormat:@"%ld",(long)[[NSDate date] timeIntervalSince1970]];
    m_file_name = [NSString stringWithFormat:@"%@.%@", m_time_name, @"pdf"];
    m_file_path = [NSString stringWithFormat:@"%@/%@", m_document_directory, m_file_name];
    m_img = [UIImage imageNamed:m_file_path];
    NSLog(@"paths : %@", m_file_path);
    
    if([m_image_data writeToFile:m_file_path atomically:YES])
        NSLog(@"writed.");
    else
        NSLog(@"Not writed.");
    
    m_orig_img_saved = true;
    
//    resultView = [[ResultView alloc] init];
//    [self presentViewController:resultView animated:YES completion:^{
   // }];
    //[self performSegueWithIdentifier:@"ResultView" sender:self];
}

- (void)saveImage
{
    //[self performSelectorInBackground:@selector(saveImageInBackground) withObject:nil];
    [self saveImageInBackground];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)saveImageInBackground
{
    NSLog(@"saveImageInBackground");
    
    @try
    {
        NSString *msg = [NSString stringWithFormat:@"Full imge saved : %@", ([UIImagePNGRepresentation(m_img_to_save) writeToFile:m_file_path atomically:YES] ? @"YES": @"NO")];
        
        m_orig_img_saved = true;
        NSLog(@"%@", msg);
    }
    @catch(NSException *exception)
    {
        NSLog(@"Exception Process.saveImage : %@", exception);
    }
}


- (UIImage *)rotation_image: (UIImage *)_img withAngle: (int)_angle
{
    NSLog(@"rotation_image");
    
    CGImageRef img_ref = _img.CGImage;
    
    CGFloat width = CGImageGetWidth(img_ref);
    CGFloat height = CGImageGetHeight(img_ref);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize image_size = CGSizeMake(CGImageGetWidth(img_ref), CGImageGetHeight(img_ref));
    CGFloat bound_height = bounds.size.height;
    bounds.size.height = bounds.size.width;
    bounds.size.width = bound_height;
    transform = CGAffineTransformMakeTranslation(image_size.height, 0.0);
    
    if(_angle == 90 || _angle == 270)
        transform = CGAffineTransformRotate(transform, M_PI / 2.0);
    else
        transform = CGAffineTransformRotate(transform, M_PI * 1.5);
    
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmap_data;
    int             bitmap_byte_count;
    int             bitmap_bytes_per_row;
    
    bitmap_bytes_per_row   = (bounds.size.width * 4);
    bitmap_byte_count     = (bitmap_bytes_per_row * bounds.size.height);
    bitmap_data = malloc( bitmap_byte_count );
    
    if (bitmap_data != NULL)
    {
        CGColorSpaceRef color_space = CGImageGetColorSpace(img_ref);
        context = CGBitmapContextCreate (bitmap_data,
                                         bounds.size.width,
                                         bounds.size.height,
                                         8,
                                         bitmap_bytes_per_row,
                                         color_space,
                                         kCGImageAlphaPremultipliedLast);
        CGColorSpaceRelease(color_space);
        
        if (context != NULL) // error creating context
        {
            CGContextScaleCTM(context, -1.0, -1.0);
            CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
            
            CGContextConcatCTM(context, transform);
            
            CGContextDrawImage(context, CGRectMake(0,0,width, height), img_ref);
            
            CGImageRef img_ref_2 = CGBitmapContextCreateImage(context);
            CGContextRelease(context);
            free(bitmap_data);
            
            _img = [UIImage imageWithCGImage:img_ref_2 scale:_img.scale orientation:UIImageOrientationUp];
            CGImageRelease(img_ref_2);
        }
    }
    return _img;
}


//---------------------------------------------------------------------------------------------------------------------------------

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return [super shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}




@end
