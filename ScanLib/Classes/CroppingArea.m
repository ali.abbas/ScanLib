//
//  CroppingArea.m
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//

#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
#import "CroppingArea.h"

@implementation CroppingArea

- (id) init
{
    self = [super init];
    
    if(self)
    {
        self.m_nb_pts = 0 ;
        self.m_nb_pane = 1 ;
        self.m_folding_orientation = @"V" ;
        self.m_pts = nil ;
    }
    
    return self;
};

- (void) RotatePts:(int)_h
{
    NSMutableArray * tmp_pts = [[NSMutableArray alloc] init];
    
    NSValue * new_pt = nil ;
    for(int a = 0; a < self.m_nb_pts; a++)
    {
        new_pt = [NSValue valueWithCGPoint:CGPointMake(0,  0)]; 
        [tmp_pts addObject:new_pt];
    }
    
    int new_pos = 0 ;
    int off_min = self.m_nb_pane + 2 ;
    
    int x_tmp = 0 ;
    
    for(int i = 0; i < self.m_nb_pts; i++)
    {
        CGPoint pt ;
        [[self.m_pts objectAtIndex:i] getValue:&pt];
        
        x_tmp = _h - pt.y ;
        pt.y  = pt.x ;
        pt.x = x_tmp ;
        
        new_pos = i + 1 - off_min ;
        if(new_pos < 0)
            new_pos += self.m_nb_pts ;
        
        [tmp_pts setObject:[NSValue valueWithCGPoint:pt] atIndexedSubscript:new_pos];
    }
    self.m_pts = tmp_pts ;
}


- (void) SetPts:(NSMutableDictionary *)_json_ca
{
    self.m_nb_pane = [[_json_ca objectForKey:@"nbPane"] intValue] ;
    self.m_folding_orientation = [_json_ca objectForKey:@"foldingOrientation"];
    
    self.m_nb_pts = self.m_nb_pane * 2 + 2 ;
    self.m_pts = [[NSMutableArray alloc]init];
    
    for(int i = 1; i <= self.m_nb_pts; i++)
    {
        int x = [[_json_ca objectForKey:[NSString stringWithFormat:@"x%d", i]] intValue] ;
        int y = [[_json_ca objectForKey:[NSString stringWithFormat:@"y%d", i]] intValue] ;
        
        NSValue * pt = [NSValue valueWithCGPoint:CGPointMake(x,  y)];
        [self.m_pts addObject:pt];
    }
}


@end
