//
//  CadreView.m
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 18/04/2016.
//
//

#import "CadreView.h"
#import "ScanLib.h" 

@implementation CadreView

//---------------------------------------------------------------------------------------------------------------------------------

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        
        // Initialization code
        self.orig_h = (int)frame.size.height ;
        self.orig_w = (int)frame.size.width ;
        self.stroke_width = self.orig_w / 200 ;
        self.scale_fact = 1.0f ;
        self.num_pt_mv = -1 ;
        self.m_moving = false ;
        self.ls_pts_d = [[NSMutableArray alloc]init];
        self.crop = nil ;
    }
    return self;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) drawRect:(CGRect)rect
{
    CGContextRef m_context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(m_context, (CGFloat)self.stroke_width);
    CGContextSetFillColorWithColor(m_context, [[UIColor yellowColor] CGColor]);
    CGContextSetStrokeColorWithColor(m_context, [[UIColor greenColor] CGColor]);
    
    int pt_size  = self.stroke_width * 20 ;
    int i_pt = [self.crop m_nb_pts] - 1 ;
    
    for(int i = 0; i < [[self crop] m_nb_pts]; i++)
    {
        CGPoint pt ;
        [[[self ls_pts_d] objectAtIndex:i] getValue:&pt];
        
        if (pt.x > cadrImg.bounds.size.width - _off_x)
            pt.x = cadrImg.bounds.size.width - _off_x;
        else if (pt.x < _off_x)
            pt.x = _off_x;
        
        if (pt.y > cadrImg.bounds.size.height - _off_y)
            pt.y = cadrImg.bounds.size.height - _off_y;
        else if (pt.y < _off_y)
            pt.y = _off_y;
        
        [self.ls_pts_d setObject:[NSValue valueWithCGPoint:CGPointMake(pt.x, pt.y)] atIndexedSubscript:i];
    }
    
    CGPoint pt ;
    [[self.ls_pts_d objectAtIndex:i_pt] getValue:&pt];
    CGContextMoveToPoint(m_context, (CGFloat)(pt.x), (CGFloat)(pt.y));
    
    for(int i = 0; i < [self.crop m_nb_pts]; i++)
    {
        [[self.ls_pts_d objectAtIndex:i] getValue:&pt];
        CGContextAddLineToPoint(m_context, pt.x, pt.y);
    }

    
    CGContextStrokePath(m_context);
    CGContextDrawPath(m_context, kCGPathStroke);
    
    for(int i = 0; i < [self.crop m_nb_pts]; i++)
    {
        [[self.ls_pts_d objectAtIndex:i] getValue:&pt];
        
        if(i != self.num_pt_mv)
        {
            CGContextFillEllipseInRect(m_context, CGRectMake(pt.x - (pt_size / 2), pt.y - (pt_size / 2), pt_size, pt_size));
        }
        else
        {
            CGContextStrokeEllipseInRect(m_context, CGRectMake(pt.x - (pt_size * 4), pt.y - (pt_size * 4), pt_size * 8, pt_size * 8));
            CGContextStrokeEllipseInRect(m_context, CGRectMake(pt.x - (pt_size / 2), pt.y - (pt_size / 2), pt_size, pt_size));
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (int)maj_index_draw:(int)_initial_index
{
    //int tmp = _initial_index - m_rotate_angle / 90;
    int tmp = _initial_index - (m_rotation_ok ? self.crop.m_nb_pane : 0);

    if(tmp < 0)
        tmp = self.crop.m_nb_pts + tmp;
    else if (tmp > self.crop.m_nb_pts - 1)
        tmp = tmp - self.crop.m_nb_pts;
    
    return tmp;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint cur_loc = [touch locationInView:self];
    
    int mX = cur_loc.x ;
    int mY = cur_loc.y ;
    
    // calcul si proche d'un point
    CGPoint pt ;
    [[self.ls_pts_d objectAtIndex:0] getValue:&pt];
    
    int rayon = 100 ;
    int rayon_min = sqrt((pt.x - mX) * (pt.x - mX) + (pt.y - mY) * (pt.y - mY));
    
    if(rayon_min < rayon)
        self.num_pt_mv = 0 ;
    
    for(int i = 0; i < [self.crop m_nb_pts]; i++)
    {
        [[self.ls_pts_d objectAtIndex:i] getValue:&pt];
        int rayon_pt = sqrt((pt.x - mX) * (pt.x - mX) + (pt.y - mY) * (pt.y - mY));
        
        if(rayon_pt < rayon &&  rayon_pt < rayon_min)
        {
            rayon_min = rayon_pt ;
            self.num_pt_mv = i ;
        }
    }
    
    if (self.num_pt_mv != -1)
    {
        self.m_moving = true;
        CGPoint pt ;
        [[self.ls_pts_d objectAtIndex:self.num_pt_mv] getValue:&pt];
        self.m_dist_x = (int) (mX - pt.x);
        self.m_dist_y = (int) (mY - pt.y);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.m_moving)
    {
        self.m_dist_x = 0;
        self.m_dist_y = 0;
        
        self.m_moving = false;
        self.num_pt_mv = -1;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.m_moving)
    {
        self.m_dist_x = 0;
        self.m_dist_y = 0;
        
        self.m_moving = false;
        self.num_pt_mv = -1;
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.m_moving)
    {
        UITouch *touch = [touches anyObject];
        CGPoint pt = [touch locationInView:self];
        
        pt.x = (int) pt.x - self.m_dist_x;
        pt.y = (int) pt.y - self.m_dist_y;
        [self.ls_pts_d setObject:[NSValue valueWithCGPoint:pt] atIndexedSubscript:self.num_pt_mv];
        
        pt.x = (pt.x - self.off_x) * self.scale_fact ;
        pt.y = (pt.y - self.off_y) * self.scale_fact ;
        [self.crop.m_pts setObject:[NSValue valueWithCGPoint:pt] atIndexedSubscript:self.num_pt_mv];
        
        [self setNeedsDisplay];
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) init_points:(CroppingArea *)crop
{
    self.crop = crop ;
    self.ls_pts_d = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < [self.crop m_nb_pts]; i++)
    {
        CGPoint pt ;
        [[self.crop.m_pts objectAtIndex:i] getValue:&pt];
        
        NSValue * npt = [NSValue valueWithCGPoint:CGPointMake(pt.x,  pt.y)];
        [self.ls_pts_d addObject:npt];
    }
    
    [self setNeedsDisplay];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) setCoords:(CroppingArea *)crop _orig_w:(float)orig_w _orig_h:(float)orig_h
{
    self.orig_w = orig_w ;
    self.orig_h = orig_h ;
    
    [self init_points:crop];
    [self maj_pts_d];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) maj_pts_d
{
    for(int i = 0; i < [self.crop m_nb_pts]; i++)
    {
        CGPoint pt ;
        [[self.crop.m_pts objectAtIndex:i] getValue:&pt];
        
        CGPoint pt2 ;
        pt2.x = pt.x / self.scale_fact + self.off_x ;
        pt2.y = pt.y / self.scale_fact + self.off_y ;
        
        [self.ls_pts_d setObject:[NSValue valueWithCGPoint:pt2] atIndexedSubscript:i];
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

//---------------------------------------------------------------------------------------------------------------------------------

-(BOOL)shouldAutorotate {
    return NO;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) rotate_coord
{
    [self.crop RotatePts:self.orig_h];
    
    int tmp = self.orig_h ;
    self.orig_h = self.orig_w ;
    self.orig_w = tmp ;
    
    //bool rot = (m_did_one_time && (m_nb_rot_before_orient_ok == 1 || m_nb_rot_before_orient_ok == 3)) != m_rotation_ok;
    int number = m_rotation_ok ? self.crop.m_nb_pane : 1;
    
    for(int a = 0; a < number; a ++) // number >= 1 for support id 3
    {
        CGPoint first_point;
        [[self.crop.m_pts objectAtIndex:0] getValue:&first_point];
        
        for(int i = 0; i < self.crop.m_nb_pts - 1; i++)
        {
            CGPoint pt;
            [[self.crop.m_pts objectAtIndex:(i + 1)] getValue:&pt];
            [self.crop.m_pts setObject:[NSValue valueWithCGPoint:CGPointMake(pt.x,  pt.y)] atIndexedSubscript:i];
        }
        
        [self.crop.m_pts setObject:[NSValue valueWithCGPoint:CGPointMake(first_point.x,  first_point.y)] atIndexedSubscript:self.crop.m_nb_pts - 1];
    }
    
    [self maj_pts_d];
    
    [self setNeedsDisplay];
}

@end

