//
//  CropView.h
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "UIKit/UIKit.h"
#import <ImageIO/ImageIO.h>


@protocol CropViewDelegate <NSObject>
@required
-(void) croppingPoints:(NSString *) cropping_area WithImage:(UIImage *) imageRecipe; 
@end

@interface CropView : UIViewController
{
    
}

@property (nonatomic, weak) id<CropViewDelegate> delegate;

@property(nonatomic, strong) UIButton * validCropBtn; 
@property (weak, nonatomic) IBOutlet UIButton *m_back_button; ///< a UIButton variable used to go back to MenuView view.
@property (weak, nonatomic) IBOutlet UIButton *m_rot_button; ///< a UIButton variable used to rotate the picture and all points.
@property (weak, nonatomic) IBOutlet UIButton *m_valid_crop_button; ///< a UIButton variable used to valid the crop and go to the result view.




extern int m_rot_button_height;
extern double m_ratio_x, m_ratio_y;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn IBAction backButton:(id)sender
 * \brief back button controller.
 *
 * This function allows to go back in MenuView view.
 *
 * \param sender an id variable.
 */
- (IBAction)backButton:(id)sender;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn IBAction validCropButton:(id)sender
 * \brief valid crop button controller.
 *
 * This function allows to call "validCrop" function.
 *
 * \param sender an id variable.
 */
- (IBAction)validCropAction:(UIButton *)sender; 
//- (IBAction)validCropButton:(id)sender;

- (void) validCropButton:(id)sender;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void validCrop
 * \brief valid crop.
 *
 * This function allows to valid the crop and go the ResultView view to get the result.
 */
- (void)validCrop;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn IBAction rotButton:(id)sender
 * \brief rot button controller.
 *
 * This function allows to make a rotation of the image and of all points drawn by calling "rotate_coords" function.
 *
 * \param sender an id variable.
 */
- (IBAction)rotButton:(id)sender;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void rotate_coords
 * \brief rotate coordinates.
 *
 * This function allows to rotate all points drawn by updating offsets with "maj_coords_offsets" function and calling "rotate_coord" function in CadreView class.
 */
- (void) rotate_coords;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void maj_coords_offsets
 * \brief update offsets.
 *
 * This function allows to up to date offsets between the image and the limits of the screen (black spaces).
 */
- (void) maj_coords_offsets;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void resizeButtons
 * \brief resizing buttons according to the screen size.
 *
 * This function allows resize buttons in the view according to the size of the screen.
 */
- (void)resizeButtons;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void createCadreView
 * \brief create cadre view.
 *
 * This function is called when the crop is done and allows to initialize cadrImg variable to draw the rectangle according to the ImageView container.
 */
- (void)createCadreView;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void validPicture:(UIImage *)img
 * \brief valid the picture.
 *
 * This function allows valid the image, calculate the crop image according to it, and launch the process to get the crop.
 *
 * \param img an UIImage variable containing the valid image to process.
 */
- (void)validPicture:(UIImage *)img;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void receivedNotification:(NSNotification *) notification
 * \brief receive dismiss notification.
 *
 * This function allows to dismiss the view when a notification is received.
 */
- (void) receivedNotification:(NSNotification *) notification;

@end

