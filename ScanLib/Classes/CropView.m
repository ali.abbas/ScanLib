//
//  CropView.m
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//
//

#import "CropView.h"
#import "PhotoView.h" 
#import "ScanLib.h"
#import "Process.h"

@interface CropView ()

@end

@implementation CropView 

double
m_ratio_x, ///< a double variable used to store x ratio
m_ratio_y; ///< a double variable used to store y ratio

int
m_rot_button_height, ///< a int variable used to store the height of rotation button.
orig_w, ///< a int variable used to store original width of the image.
orig_h, ///< a int variable used to store original height of the image.
redim_w, ///< a int variable used to store redimensioned width of the image.
redim_h; ///< a int variable used to store redimensioned height of the image.


int m_rotate_angle = 0; ///< an int variable used to store the angle of rotation of the image.


bool
m_can_spinner = true, ///< a bool variable used to know if we can add a loading spinner or not.
//m_rotation_ok = false, ///< a bool variable used to know if we rotate the image or not (90° and 270° = yes ; 0° and 180° and 360° = no).
m_did_one_time = false; ///< a boolean containing true if a rotation has been made one time, false if 0. This is used to force the system to take in count the fact that when a rotation is made before the crop, we need to inverse the process only in some places. // TODO do something more simple to understand...




UIImageView *m_img_vw_crop; ///< a UIImageView variable used to store and display crop image.
NSString *TAG_CV = @"CropView - "; ///< a NSString variable used to know in which class is executed such function.

//---------------------------------------------------------------------------------------------------------------------------------

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@viewDidLoad", TAG_CV);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"dismissViewController"
                                               object:nil];
    
    
   
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    m_rotation_ok = false;
    m_rotate_angle = 0;
    m_crop = true;
    m_did_one_time = false;

    [self resizeButtons];
    m_rot_button_height = _m_rot_button.bounds.size.height;
    
    m_img_vw_crop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height - m_rot_button_height)];
    m_img_vw_crop.contentMode = UIViewContentModeScaleAspectFit ;
    m_img_vw_crop.backgroundColor = [UIColor blackColor];
    m_img_vw_crop.hidden = true;
    
    
    [self.view addSubview:m_img_vw_crop];
    [self validPicture:m_img];
}

-(void) addCropButton {
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    self.validCropBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, height-24, width, 24)];
    //self.validCropBtn.frame = CGRectMake(0, height-24, width, 24);
    self.validCropBtn.opaque = false ;
    self.validCropBtn.userInteractionEnabled = true;
    [self.validCropBtn setTitle:(NSString *)@"Valider Crop" forState:(UIControlState)UIControlStateNormal];
    [self.validCropBtn addTarget:self action:@selector(validCropButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.validCropBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of my ressources tha can be created
}

//---------------------------------------------------------------------------------------------------------------------------------

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

//---------------------------------------------------------------------------------------------------------------------------------

-(BOOL)shouldAutorotate
{
    return NO;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (BOOL)prefersStatusBarHidden {
    return YES;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (IBAction)backButton:(id)sender
{
    NSLog(@"%@backButton", TAG_CV);
    
    _m_rot_button.enabled = NO;
    _m_valid_crop_button.enabled = NO;
    _m_back_button.enabled = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//---------------------------------------------------------------------------------------------------------------------------------
- (void) validCropButton:(id)sender
{
    NSLog(@"%@validCropButton", TAG_CV);
    
    // Save the image with the right orientation only if rotation value != 0 or != 360.
    // and continue only when the image is saved,
    
    _m_rot_button.enabled = NO;
    _m_valid_crop_button.enabled = NO;
    _m_back_button.enabled = NO;
    

    [self validCrop];
    
}


//---------------------------------------------------------------------------------------------------------------------------------

- (void)validCrop
{
    NSLog(@"%@validCrop", TAG_CV);
    _m_valid_crop_button.enabled = NO;
    
    if(m_orig_img_saved)
    {
        Process *p = [[Process alloc] init];
        [self.delegate croppingPoints:[p process] WithImage:m_img];  
        NSLog(@"%@ : ",[p process]);
    }
    else
    {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(100 * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^{
            [self validCrop]; 
        });
    }
}

//---------------------------------------------------------------------------------------------------------------------------------

- (IBAction)rotButton:(id)sender
{
    NSLog(@"%@rotButton", TAG_CV);
    
    self.m_rot_button.enabled = NO;
    _m_valid_crop_button.enabled = NO;
    _m_back_button.enabled = NO;
    
    m_rotation_ok = !m_rotation_ok;
    
    // rotation of the image and of all points (n).
    if(m_rotate_angle == 270)
        m_rotate_angle = 0 ;
    else
        m_rotate_angle += 90 ;
    
    m_img_vw_crop.image = [self rotation_image:m_img_vw_crop.image withAngle:90];
    
    [self rotate_coords];

    _m_rot_button.enabled = YES;
    _m_valid_crop_button.enabled = YES;
    _m_back_button.enabled = YES;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) rotate_coords
{
    NSLog(@"%@rotate_coords", TAG_CV);
    
    float tmp = orig_w ;
    orig_w = orig_h ;
    orig_h = tmp ;
    
    tmp = redim_h ;
    redim_h = redim_w ;
    redim_w = tmp ;
    
    [self maj_coords_offsets];
    
    [cadrImg rotate_coord];
    m_did_one_time = true;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) maj_coords_offsets
{
    NSLog(@"%@maj_coords_offsets", TAG_CV);
    
    int disp_w = m_img_vw_crop.frame.size.width ;
    int disp_h = m_img_vw_crop.frame.size.height ;
    
    float widthFactor = redim_w / (float)disp_w ;
    float heightFactor = redim_h / (float)disp_h ;
    
    if (widthFactor > heightFactor)
    {
        disp_h = redim_h / widthFactor ;
        cadrImg.off_y = (m_img_vw_crop.frame.size.height - disp_h) / 2 ;
        cadrImg.off_x = 0 ;
    }
    else
    {
        disp_w = redim_w / heightFactor ;
        cadrImg.off_x = (m_img_vw_crop.frame.size.width - disp_w) / 2 ;
        cadrImg.off_y = 0 ;
    }
    cadrImg.scale_fact = orig_w / (float)disp_w ;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)resizeButtons
{
    NSLog(@"%@resizeButtons", TAG_CV);
    
    double scr_w = (double)[UIScreen mainScreen].bounds.size.width;
    float x = self.m_back_button.frame.origin.x;
    float y = self.m_back_button.frame.origin.y;
    float width = (float)(scr_w * 0.40);
    float height = self.m_back_button.frame.size.height;
    self.m_back_button.frame = CGRectMake(x, y, width, height); // back button
    
    x = (float)(scr_w * 0.41);
    width = (float)(scr_w * 0.18);
    self.m_rot_button.frame = CGRectMake(x, y, width, height); // rot button
    
    UIImage *btn_image = [UIImage imageNamed:[[NSBundle mainBundle] pathForResource:@"rot_img" ofType:@"png"]];
    [self.m_rot_button setImage:btn_image forState:UIControlStateNormal]; // setting rotation image to rot_button
    
    x = (float)(scr_w * 0.60);
    width = (float)(scr_w * 0.40);
    self.m_valid_crop_button.frame = CGRectMake(x, y, width, height); // valid crop button
    
    self.m_back_button.enabled = NO;
    self.m_rot_button.enabled = NO;
    self.m_valid_crop_button.enabled = NO;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)createCadreView
{
    NSLog(@"%@createCadreView", TAG_CV);

    orig_w = m_img_to_save.size.width ;
    orig_h = m_img_to_save.size.height ;
    
    int dest_w = m_img_vw_crop.bounds.size.width ;
    int dest_h = m_img_vw_crop.bounds.size.height ;

    float widthFactor = m_img_to_save.size.width / dest_w ;
    float heightFactor = m_img_to_save.size.height / dest_h ;
    
    if (widthFactor > heightFactor)
        dest_h = m_img_to_save.size.height / widthFactor ;
    else
        dest_w = m_img_to_save.size.width / heightFactor ;
    
    CGSize size = CGSizeMake(dest_w, dest_h);
    UIGraphicsBeginImageContext( size);
    [m_img_to_save drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *redimImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    redim_w = redimImg.size.width ;
    redim_h = redimImg.size.height ;
    
    CroppingArea *cropping_area = [[CroppingArea alloc ] init]; 
    
    // Preparing points
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    NSNumber *nb_pane = @1;
    if([m_cropping valueForKey:@"nbPane"] != nil)
        nb_pane = [m_cropping valueForKey:@"nbPane"];
    [dic setObject:nb_pane forKey:@"nbPane"];
    
    NSString *foldingOrientation = @"H";
    if([m_cropping valueForKey:@"foldingOrientation"] != nil)
        foldingOrientation = [m_cropping valueForKey:@"foldingOrientation"];
    [dic setObject:foldingOrientation forKey:@"foldingOrientation"];
    
    int nb_points = [nb_pane intValue] * 2 + 2;
    for(int i = 1; i < nb_points + 1; i++)
    {
        int x = [[m_cropping valueForKey:[NSString stringWithFormat:@"x%i", i]] integerValue] * m_ratio_x;
        [dic setObject:[NSNumber numberWithInt:x] forKey:[NSString stringWithFormat:@"x%i", i]];
        int y = [[m_cropping valueForKey:[NSString stringWithFormat:@"y%i", i]] integerValue] * m_ratio_y;
        [dic setObject:[NSNumber numberWithInt:y] forKey:[NSString stringWithFormat:@"y%i", i]];
    }
    m_cropping = [dic mutableCopy];
    
    [cropping_area SetPts:[m_cropping mutableCopy]];
    
    cadrImg = [[CadreView alloc] initWithFrame:m_img_vw_crop.frame];
    [self maj_coords_offsets];
    [cadrImg setCoords:cropping_area _orig_w:orig_w _orig_h:orig_h];
    [self.view addSubview:cadrImg];
    [self.view bringSubviewToFront:cadrImg];
    
    [self addCropButton];
    [cadrImg addSubview:self.validCropBtn];
    //[cadrImg bringSubviewToFront:self.validCropBtn];
    
    self.m_back_button.enabled = YES;
    self.m_rot_button.enabled = YES;
    self.m_valid_crop_button.enabled = YES; 
    
    //m_img_vw_crop.image = redimImg;
    //[m_tools loadingSpinner:self.view:false];
}


- (UIImage *)rotation_image: (UIImage *)_img withAngle: (int)_angle
{
    NSLog(@"%@rotation_image");
    
    CGImageRef img_ref = _img.CGImage;
    
    CGFloat width = CGImageGetWidth(img_ref);
    CGFloat height = CGImageGetHeight(img_ref);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    CGSize image_size = CGSizeMake(CGImageGetWidth(img_ref), CGImageGetHeight(img_ref));
    CGFloat bound_height = bounds.size.height;
    bounds.size.height = bounds.size.width;
    bounds.size.width = bound_height;
    transform = CGAffineTransformMakeTranslation(image_size.height, 0.0);
    
    if(_angle == 90 || _angle == 270)
        transform = CGAffineTransformRotate(transform, M_PI / 2.0);
    else
        transform = CGAffineTransformRotate(transform, M_PI * 1.5);
    
    // Create the bitmap context
    CGContextRef    context = NULL;
    void *          bitmap_data;
    int             bitmap_byte_count;
    int             bitmap_bytes_per_row;
    
    bitmap_bytes_per_row   = (bounds.size.width * 4);
    bitmap_byte_count     = (bitmap_bytes_per_row * bounds.size.height);
    bitmap_data = malloc( bitmap_byte_count );
    
    if (bitmap_data != NULL)
    {
        CGColorSpaceRef color_space = CGImageGetColorSpace(img_ref);
        context = CGBitmapContextCreate (bitmap_data,
                                         bounds.size.width,
                                         bounds.size.height,
                                         8,
                                         bitmap_bytes_per_row,
                                         color_space,
                                         kCGImageAlphaPremultipliedLast);
        CGColorSpaceRelease(color_space);
        
        if (context != NULL) // error creating context
        {
            CGContextScaleCTM(context, -1.0, -1.0);
            CGContextTranslateCTM(context, -bounds.size.width, -bounds.size.height);
            
            CGContextConcatCTM(context, transform);
            
            CGContextDrawImage(context, CGRectMake(0,0,width, height), img_ref);
            
            CGImageRef img_ref_2 = CGBitmapContextCreateImage(context);
            CGContextRelease(context);
            free(bitmap_data);
            
            _img = [UIImage imageWithCGImage:img_ref_2 scale:_img.scale orientation:UIImageOrientationUp];
            CGImageRelease(img_ref_2);
        }
    }
    return _img;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)validPicture:(UIImage *)img
{
    NSLog(@"%@validPicture", TAG_CV);

    m_img_vw_crop.image = img;
    m_img_vw_crop.hidden = NO;
    m_background_process_ok_with_error = @"";
    
    [self imageConfirmed];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)imageConfirmed
{
    CGSize dst_size = CGSizeMake(600, 800);
    UIGraphicsBeginImageContext(dst_size);
    [m_img_vw_crop.image drawInRect:CGRectMake(0, 0, dst_size.width, dst_size.height)];
    UIImage *img_crop = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext(); // resize picture
    
    if([UIImagePNGRepresentation(img_crop) writeToFile:m_file_path_crop atomically:YES]) // save small size image for crop.
        NSLog(@"writed. (crop)");
    else
        NSLog(@"Not writed. (crop)");
    
    m_image_data = UIImageJPEGRepresentation(img_crop, 1);
    
    m_ratio_y = m_img_vw_crop.image.size.height / img_crop.size.height;
    m_ratio_x = m_img_vw_crop.image.size.width / img_crop.size.width;
    
    [self createCadreView];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)waitUntilCropEnds
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(200 * NSEC_PER_MSEC)), dispatch_get_main_queue(), ^
                   {

                           NSLog(@"%@waitUntilCropEnds", TAG_CV);
                           m_background_process_ok = false;
                           
                           if([m_background_process_ok_with_error isEqualToString:@""])
                           {
                               NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                               
                               NSNumber *nb_pane = @1;
                               if([m_cropping valueForKey:@"nbPane"] != nil)
                                   nb_pane = [m_cropping valueForKey:@"nbPane"];
                               [dic setObject:nb_pane forKey:@"nbPane"];
                               
                               NSString *foldingOrientation = @"H";
                               if([m_cropping valueForKey:@"foldingOrientation"] != nil)
                                   foldingOrientation = [m_cropping valueForKey:@"foldingOrientation"];
                               [dic setObject:foldingOrientation forKey:@"foldingOrientation"];
                               
                               int nb_points = [nb_pane intValue] * 2 + 2;
                               for(int i = 1; i < nb_points + 1; i++)
                               {
                                   int x = [[m_cropping valueForKey:[NSString stringWithFormat:@"x%i", i]] integerValue] * m_ratio_x;
                                   [dic setObject:[NSNumber numberWithInt:x] forKey:[NSString stringWithFormat:@"x%i", i]];
                                   int y = [[m_cropping valueForKey:[NSString stringWithFormat:@"y%i", i]] integerValue] * m_ratio_y;
                                   [dic setObject:[NSNumber numberWithInt:y] forKey:[NSString stringWithFormat:@"y%i", i]];
                               }
                               m_cropping = [dic mutableCopy];
                               [self createCadreView];
                           }
                           else
                           {
                               NSLog(@"Error : %@", m_background_process_ok_with_error);
                              // [m_tools displayToastWithMessage:m_background_process_ok_with_error];
                              // [m_tools loadingSpinner:self.view:false];
                               [self dismissViewControllerAnimated:YES completion:nil];
                           }
                           
                   });
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void) receivedNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"dismissViewController"])
        [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)validCropAction:(UIButton *)sender {
    
    NSLog(@"%@validCropButton", TAG_CV);
    
    // Save the image with the right orientation only if rotation value != 0 or != 360.
    // and continue only when the image is saved,
    
    _m_rot_button.enabled = NO;
    _m_valid_crop_button.enabled = NO;
    _m_back_button.enabled = NO;
    
    //if(!m_orig_img_saved)
    //  [m_tools loadingSpinner:self.view :true];
    
    [self validCrop];
}
@end
