//
//  Traitment.m
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//
//

#import "Process.h"
#import "ScanLib.h"

@implementation Process

//---------------------------------------------------------------------------------------------------------------------------------

- (NSString *)process
{
    NSString *m_cropping_area = @"";
    CGPoint pt;
    m_cropping_area = @"\"croppingArea\":{";
    for(int i = 0; i < cadrImg.crop.m_nb_pts; i ++)
    {
        [[cadrImg.crop.m_pts objectAtIndex:i] getValue:&pt];
        m_cropping_area = [NSString stringWithFormat:@"%@\"x%i\":%.f,", m_cropping_area, (i + 1), pt.x];
        m_cropping_area = [NSString stringWithFormat:@"%@\"y%i\":%.f,", m_cropping_area, (i + 1), pt.y];
    }
//    m_cropping_area = [NSString stringWithFormat:@"%@\"nbPane\":%i,", m_cropping_area, cadrImg.crop.m_nb_pane];
//    m_cropping_area = [NSString stringWithFormat:@"%@\"foldingOrientation\":\"%@\"}", m_cropping_area, cadrImg.crop.m_folding_orientation];
    return m_cropping_area;
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)saveImage
{
    //[self performSelectorInBackground:@selector(saveImageInBackground) withObject:nil];
    [self saveImageInBackground];
}

//---------------------------------------------------------------------------------------------------------------------------------

- (void)saveImageInBackground
{
    @try
    {
        NSString *msg = [NSString stringWithFormat:@"Full imge saved : %@", ([UIImagePNGRepresentation(m_img_to_save) writeToFile:m_file_path atomically:YES] ? @"YES": @"NO")];        
        m_orig_img_saved = true;
        NSLog(@"%@", msg);
    }
    @catch(NSException *exception)
    {
        NSLog(@"Exception Process.saveImage : %@", exception);
    }
}

@end

