//
//  ScanLib.h
//  Pods-ScanLib_Example
//
//  Created by ruben Boboti on 07/08/2018.
//

#import <Foundation/Foundation.h>
#import "PhotoView.h"
#import "CadreView.h" 


extern UIImage *m_img_to_save, *m_img;
extern NSData *m_image_data;
extern CadreView *cadrImg;
extern id m_cropping;

extern NSString
*m_country,
*m_result,
*m_background_process_ok_with_error,
*m_language,
*m_file_path,
*m_file_path_crop,
*m_auth_value,
*m_token;

extern bool
m_rotation_ok ,
m_orig_img_saved,
m_image_taken,
m_have_internet,
m_from_crop,
m_from_pdf_file,
m_crop,
m_background_process_ok;


@protocol ScanLibDelegate <NSObject>
@required
-(void) scanDone:(NSString *) cropping_area WithImage:(UIImage *) imageRecipe; 
@end

@interface ScanLib : UIViewController <CropViewDelegate> 
{
    UIImagePickerController *m_ipc; 
}

@property (nonatomic, weak) id<ScanLibDelegate> delegate;  

- (id)init;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void showCamera
 * \brief show camera function.
 *
 * This function allows to open the camera and launch waitUntilImageIsTaken function.
 */

- (void)openCamera;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void waitUntilImageIsTaken
 * \brief photo taking waiting loop.
 *
 * This function allows to check when the image is taken. The image is stored, rotated by 90° automatically, and the crop is done by calling CropView view.
 */

- (void)waitUntilImageIsTaken;
//---------------------------------------------------------------------------------------------------------------------------------


/**
 * \fn void imagePickerControllerDidCancel:(UIImagePickerController *)_picker
 * \brief image picker canceled controller.
 *
 * This function is called when the imagePickerController is cancelled.
 *
 * \param _picker a UIImagePickerController : the controller object managing the image picker interface.
 */
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)_picker;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void imagePickerController:(UIImagePickerController *)_picker didFinishPickingMediaWithInfo:(NSDictionary *)_info
 * \brief image picker controller.
 *
 * This function allows to control the image taken from the gallery and launch waitUntilImageIsTaken to make the crop.
 *
 * \param _picker a UIImagePickerController : the controller object managing the image picker interface.
 * \param _info a NSDictionary which includes informations of the image that was taken.
 */
- (void)imagePickerController:(UIImagePickerController *)_picker didFinishPickingMediaWithInfo:(NSDictionary *)_info;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void choosePDF
 * \brief pdf processing function.
 *
 * This function allows to treat a pdf file from an URL.
 *
 * \param _pdf_url a NSString containing the url of the pdf file.
 */
- (void)choosePDF:(NSString *)_pdf_url;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void chooseGallery
 * \brief gallery opening function.
 *
 * This function allows to open the photo library.
 */
- (void)chooseGallery;

//---------------------------------------------------------------------------------------------------------------------------------

@end
