//
//  CroppingArea.h
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 31/10/2017.
//

#import <Foundation/Foundation.h>

@interface CroppingArea : NSObject

@property (nonatomic) int m_nb_pane ; ///< a int variable used to store the number of pane of the image.
@property (nonatomic) int m_nb_pts ; ///< a int variable used to store the number of points according to the support (support_id).
@property (nonatomic) NSString * m_folding_orientation ; ///< a NSString variable used to store the folding orientation of the image.

@property (nonatomic,retain) NSMutableArray * m_pts; ///< a NSMutableArray variable used to store points of cropping area.

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn init
 * \brief init function.
 *
 * This function allows to initialize CroppinArea class.
 *
 * \return id (self).
 */
- (id) init ;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn RotatePts:(int)_h
 * \brief rotate m_pts points according to _h parameter.
 *
 * This function allows to rotate the points of m_pts variable.
 *
 * \param _h a int variable containing the original height of the ImageView container.
 */
- (void) RotatePts:(int)_h ;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn SetPts:(NSMutableDictionary *)_json_ca
 * \brief set points according to _json_ca parameter.
 *
 * This function allows to set the cropping area points and variables like nbPane and foldingOrientation with the parameter.
 *
 * \param _json_ca a NSMutableDictionary variable containing points, folding orientation and pane number informations to set.
 */
- (void) SetPts:(NSMutableDictionary *)_json_ca ;


@end
