//
//  CadreView.h
//  OcrmSample_ios
//
//  Created by SITUVE sarl on 18/04/2016.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CropView.h"
#import "CroppingArea.h"

@interface CadreView : UIView

@property (nonatomic) int orig_w ; ///< a int variable used to store the original width of the captured image.
@property (nonatomic) int orig_h ; ///< a int variable used to store the original height of the captured image.
@property (nonatomic) int off_y ; ///< a int variable used to store the y difference between the image and limits of the screen.
@property (nonatomic) int off_x ; ///< a int variable used to store the x difference between the image and limits of the screen.
@property (nonatomic) int stroke_width ; ///< a int variable used to store the stroke width which is used to display the crop
@property (nonatomic) int num_pt_mv ; ///< a int variable used to store the number of the point which is actually moving.
@property (nonatomic) int m_dist_x ; ///< a int variable used to store the x distance between the finger and the nearest point (limited with a ray).
@property (nonatomic) int m_dist_y ; ///< a int variable used to store the y distance between the finger and the nearest point (limited with a ray).

@property (nonatomic) float scale_fact ; ///< a float variable used to store the scale factor

@property (nonatomic) bool m_moving ; ///< a bool variable used to lock or unlock the movement of points when the user touch the screen.

@property (nonatomic,retain) NSMutableArray * ls_pts_d; ///< a NSMutableArray variable used to store points which allows to draw the crop on the screen.

@property (nonatomic,retain) CroppingArea * crop; ///< a CroppingArea variable variable used to store the crop's coordinates.

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn id initWithFrame:(CGRect)frame
 * \brief init CadreView with CGRect.
 *
 * This function allows to CadreView class with a CGRect.
 *
 * \param frame a CGRect variable containing the frame.
 * \return id.
 */
- (id)initWithFrame:(CGRect)frame;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void drawRect:(CGRect)rect
 * \brief draw points.
 *
 * This function allows to draw and display a rectangle with points.
 *
 * \param rect a CGRect variable containing a rectangle.
 */
- (void) drawRect:(CGRect)rect;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn int maj_index_draw:(int)_initial_index
 * \brief update the index when drawing.
 *
 * This function is called only when we are in support id 3 (car title).
 * Lines that separate the panes are shifted when we have a rotation, this is why this function allows to update the lines.
 *
 * \param _initial_index a int variable containing the initial index.
 * \return int : the updated index.
 */
- (int)maj_index_draw:(int)_initial_index;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
 * \brief when the user touches the screen.
 *
 * This function is called when the user touch the screen.
 *
 * \param touches a NSSet variable containing x and y position of the place touched by the user.
 * \param event a UIEvent variable containing more detail about this event.
 */
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
 * \brief when the touche is canceld.
 *
 * This function is called when the touch is canceled.
 *
 * \param touches a NSSet variable containing x and y position of the place touched by the user.
 * \param event a UIEvent variable containing more detail about this event.
 */
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
 * \brief when the user removes his finger.
 *
 * This function is called when the user removes his finger from the screen.
 *
 * \param touches a NSSet variable containing x and y position of the place touched by the user.
 * \param event a UIEvent variable containing more detail about this event.
 */
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
 * \brief when the user moves his finger on the screen.
 *
 * This function is called when the user moves his finger on the screen.
 *
 * \param touches a NSSet variable containing x and y position of the place touched by the user.
 * \param event a UIEvent variable containing more detail about this event.
 */
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void init_points:(CroppingArea *)crop
 * \brief init points according to the parameter.
 *
 * This function allows to init the points of the class CadreView with the parameter crop.
 *
 * \param crop a CroppingArea variable containing points of the cropping area.
 */
- (void) init_points:(CroppingArea *)crop ;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void setCoords:(CroppingArea *)crop _orig_w:(float)orig_w _orig_h:(float)orig_h
 * \brief set points according to parameters.
 *
 * This function allows to set coordinates of the class CadreView.
 *
 * \param crop a CroppingArea variable containing points of the cropping area.
 * \param orig_w a float variable containing the original width of the ImageView container which is treated.
 * \param orig_h a float variable containing the original height of the ImageView container which is treated.
 */
- (void) setCoords:(CroppingArea *)crop _orig_w:(float)orig_w _orig_h:(float)orig_h ;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void maj_pts_d
 * \brief update points.
 *
 * This function allows to set ls_pts_d variable with points of crop variable according to off_x, off_y and scale_fact variables.
 */
- (void) maj_pts_d;

//---------------------------------------------------------------------------------------------------------------------------------

/**
 * \fn void rotate_coord
 * \brief rotate coordinates.
 *
 * This function allows to rotate coordinates of points when a rotation is done. It also up to date ls_pts_d variables by calling maj_pts_d function.
 */
- (void) rotate_coord;

@end
